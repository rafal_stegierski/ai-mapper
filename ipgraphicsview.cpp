#include <QMessageBox>
#include <QPixmap>
#include <QGraphicsPixmapItem>
#include <QDebug>
#include "ipgraphicsview.h"

IPGraphicsView::IPGraphicsView()
{
    this->scene = new QGraphicsScene();
    this->setScene(this->scene);
    this->mainImage = nullptr;
}

IPGraphicsView::~IPGraphicsView()
{
    if(!this->mainImage)
        delete this->mainImage;
    delete scene;
}

void IPGraphicsView::findRoad()
{
    this->road << Position{0,0};
    this->road << Position{0,1};
    this->road << Position{1,1};
}

void IPGraphicsView::drawRoad()
{
    QPen pen;
    pen.setColor(QColor(255,0,0));
    pen.setWidth(10);
    for(int i=0; i<this->road.size()-1; i++) {
        this->scene->addLine(this->road.at(i).x*64+32, this->road.at(i).y*64+32,
                             this->road.at(i+1).x*64+32, this->road.at(i+1).y*64+32,
                             pen);
    }
}

void IPGraphicsView::resizeEvent(QResizeEvent *event)
{
    this->fitInView(this->scene->itemsBoundingRect(),
                    Qt::KeepAspectRatio);
}

void IPGraphicsView::openImageFile(const QString &file)
{
    if(!this->mainImage)
        delete this->mainImage;

    this->mainImage = new QImage(file);

    /* Konwersja do mapy */
    for(int i=0;i<64;i++)
        for(int j=0;j<64;j++){
            if((this->mainImage->pixel(i,j) & 0xffffff) == 0x00ff00) { // green
                this->scene->addRect(i*64, j*64, 64, 64)->setBrush(QColor(0,255,0));
                this->map[i][j] = MapFields::Ground;
            } else if ((this->mainImage->pixel(i,j) & 0xffffff) == 0x0000ff) { // blue
                this->scene->addRect(i*64, j*64, 64, 64)->setBrush(QColor(0,0,255));
                this->map[i][j] = MapFields::Water;
            } else if ((this->mainImage->pixel(i,j) & 0xffffff) == 0xffff00) { // yellow
                this->scene->addRect(i*64, j*64, 64, 64)->setBrush(QColor(255,255,0));
                this->map[i][j] = MapFields::Desert;
            }
        }

    findRoad();
    drawRoad();

    this->fitInView(this->scene->itemsBoundingRect(),
                    Qt::KeepAspectRatio);

    QMessageBox alert;
    alert.setText("Map file " +
                  file +
                  " info: \nresolution: "+
                  QString::number(this->mainImage->width())+
                  "x"+
                  QString::number(this->mainImage->height())+
                  "\ndepth: "+
                  QString::number(this->mainImage->depth())
    );
    alert.exec();
}
