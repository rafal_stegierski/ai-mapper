#ifndef IPGRAPHICSVIEW_H
#define IPGRAPHICSVIEW_H

#include <QGraphicsView>

enum MapFields {
    Ground,
    Water,
    Desert
};

struct Position {
    quint32 x;
    quint32 y;
};

class IPGraphicsView : public QGraphicsView
{
public:
    IPGraphicsView();
    ~IPGraphicsView();
    void findRoad();
    void drawRoad();

private:
    QGraphicsScene *scene;
    quint8 map[64][64];
    QImage *mainImage;
    void resizeEvent(QResizeEvent* event);
    QList<Position> road;

public slots:
    void openImageFile(const QString &file);
};

#endif // IPGRAPHICSVIEW_H
